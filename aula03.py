#COLEÇÕES -> Conjuntos de determinados valores - algo como uma lista de ítens

#Strings - é uma coleção de caracteres e/ou símbolos. Ex: "abcdefg"
#Tuplas, Listas, Dicionários e Sets



#--------------------------------------TUPLAS----------------------------------------------------
#Tuplas (entre parênteses) - SÃO IMUTÁVEIS

tupla1 = ("Arroz", "Feijão", 50, 44.7, False)       #Uma tupla com 5 valores diferentes
#INDEX       0         1      2    3     4
print(tupla1)                                       #Imprimir toda a tupla
print(tupla1[4])                                    #Imprimir o index 4 (False)

#podemos utilizar as coleções justamente com o comando FOR
# for var1 in range(0,10):
#     print(f"Imprimindo a {var1}a mensagem")

for valor in tupla1:
    print(f"Valor atual de x é: {valor}")           # MANEIRA DE LISTAR O QUE TEMOS EM UMA TUPLA




#--------------------------------------LISTAS----------------------------------------------------
#Listas (entre colchetes) - SÃO MUTÁVEIS

lista1 = ["Arroz", "Feijão", 50, 44.7, False]        #Uma tupla com 5 valores diferentes
#INDEX       0         1      2    3     4

print(lista1[2])                                     #Imprimir o index 2 da lista (número 50)

for valor1 in lista1:
    print(f"Valor atual de x é: {valor1}")

lista1.append("Cachorro")                         #Adicionar um novo obejeto ao final da lista
lista1.clear()                                    #Limpa todo o conteúdo da sua lista
lista1.count("Arroz")                             #Contar quantas vezes o objeto "Arroz" aparece
lista1.index("Feijão")                            #Retorna qual posição encontra-se o objeto Feijão
lista1.insert()                                   #Insere um objeto em uma determinada posição
lista1[1] = "Novo Valor"                          #Altera um valor do index 1 dentro de uma lista 
lista1.pop(0)                                     #Apaga um objeto específico pelo Index
lista1.remove("Arroz")                            #Apaga um objeto específico pelo Nome


len(lista1)   #VAI CONTAR QUANTOS OBJETOS EXISTEM DENTRO DA LISTA1
print(len(lista1))


#------------------------------------DICIONÁRIOS---------------------------------------------------
#Dicionários {entre chaves} - SÃO MUTÁVEIS
#São "Hash Tables" - Armazenam seus valores no formato Chave: Valor

#              chave : valor
dicionario1 = {"Nome":"Ricardo"}
dicionario2 = {"Idade":37}
dicionario3 = {"Altura":1.73}

# ou

#               key     value     key   value   key  value
dicionario4 = {"Nome":"Ricardo", "Idade":37, "Altura":1.73}
#                   ITEM1           ITEM2        ITEM3

print(dicionario4["Nome"])   #Para dicionários, usamos as chaves para imprimir um determinado index

for x in dicionario4:
    print(x)                 #imprime todas as chaves de um dicionário

for y in dicionario4.values():
    print(y)                 #imprime todos os valores de um dicionário

for z in dicionario4.items():
    print(z)                 #imprime todos os ítems (Chave + Valor) de uma lista

for x, y in dicionario4.items():
    print(x, y)              #imprime tods os ítens de uma lista, separado em dois objetos (x e y)


dicionario4["Peso"] = 80     #adiciona um novo ítem no dicionário
dicionario4.pop("Idade")     #apaga um determinado ítem do dicionário



# *** É possível adicionar uma lista a um dicionário, por exemplo (aninhação)

lista2 = [1, 2, 3]
dicionario5 = {"Nome":"Ricardo", "Idade":lista2, "Altura":1.73}

#*** É possível adicionar uma lista a uma outra lista (aninhação)

lista3 = [1, 2, 3["Carro", "Casa"]]


#------------------------------------SETS---------------------------------------------------
#São criados utilizando chaves, mas usando apenas valores únicos, e não duplios
#Podemos pensar como sendo um dicionário, mas contendo apenas chaves, e não valores

set1 = {"Ricardo", 1986, "Palmeiras", 1.73}

#pode ser utilizado para remover ítems repetidos de uma lista, por exemplo
lista4 = [10, 15, 12, 12, 11, 12]    #lista com ítens repetidos
lista4 = set(lista4)                 #transformando a lista em um set
lista4 = list(lista4)                #retornando o set para um a lista


