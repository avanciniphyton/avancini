# =============================================================
# 3) Escreva um programa em python que conte as vogais que a música ‘Faroeste Caboclo’
# tem em sua letra. Armazena a letra da música em um arquivo do tipo txt.
# Dica: Não se esqueça de considerar as letras maiúsculas, minúsculas e com acentuação.

import random, time

# def vogais_contador(letra):
#     vogais = set("aéeéiíoóuú")
#     numero_vogais = 0
#     for letra in texto.lower():
#         if letra in vogais:
#             numero_vogais += 1
#         return numero_vogais

def check_vogal(letra):
    if letra.lower() in "aáàeéèiíìoóòuúùãẽĩõũâêîôû":
        return True
    else:
        return False

vogais = 0

with open("faroeste.txt", "r", encoding="UTF-8") as arquivo:
    conteudo = arquivo.read()
    for letra in conteudo:
        if check_vogal(letra) == True:
            vogais += 1

print("{} vogais encontradas!".format(vogais))

# OU


