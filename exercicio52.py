# =============================================================
# 2) Crie um programa que pergunte para o usuario um numero de pessoas a participarem de um sorteio (2-20),
#  e o numero de pessoas a serem sorteadas, e depois sorteie esse numero de pessoas da lista.

# O programa deverá pegar o numero de pessoas a participar aleatoriamente desta lista:

lista = ["Joao", "Maria", "Tiago", "Amanda", "Emanuele", "Caio", "Suzana", "Miguel", 
"Rosangela", "Rian", "Lucimar", "Ulisses", "Leonardo", "Kaique", "Bruno", "Raquel", 
"Benedito", "Tereza", "Valmir", "Joaquim"]

# Nota: A mesma pessoa não pode ganhar duas vezes.

import random

participantes = int(input("Quantas pessoas irão participar do sorteio (2 a 20 pessoas )"))
sorteios = int(input("Quandos sorteios serão realizados? "))

random.shuffle(lista)

vencedores_sorteados = lista[:sorteios]

for vencedor in vencedores_sorteados:
    print("Os vencedor é:")
    print(f"  - {vencedor}")