# ====================================================================================
# 4) Escreva um programa em python que realize um cadastro. Deverão ser coletadas as
# seguintes informações:

# CPF
# Nome
# Idade
# Sexo
# Endereço

# Os registros deverão ser armazenados em um arquivo CSV. Caso desejar manter o padrão
# brasileiro, o CSV será separado pelo caractere ;

import csv

while True:
    opcao = input("Gostaria de cadastrar uma pessoa nova no banco de dados? [S/N]")

    if opcao.upper() == "N":
        break

    elif opcao.upper() == "S":
        cpf = input("Qual é o seu CPF? ")
        nome = input("Qual é o seu nome? ")
        idade = input("Qual é a sua idade? ")
        sexo = input("Qual é o seu sexo? ")
        address = input("Qual é o seu endereço? ")  

        lista_nomes = (cpf, nome, idade, sexo, address)

        with open("cadastro.csv", "a") as arquivo:
            conteudo = csv.writer(arquivo, delimiter=";")
            conteudo.writerow(lista_nomes)

    else:
        print("Opção inválida")




