# Exercicio 3:
# Escreva um programa que possua uma função que conte o
# numero de números pares passados à ela, pelo usuário.

# lista1 = []
# lista2 = []
# opt = 0

# while True:
#     print(f"""
#           Vamos separar os números pares!
#           1: Adicionar um número (diferente de zero)
#           2: Mostrar números pares
#           0: Sair """)
#     opt=int(input("Escolha a sua opção: "))

#     if opt == 1:
#         lista1.append(int(input("Me diga qual o número: ")))
    
#     if opt == 2:
#         for valor in lista1:
#             if valor % 2 == 0:
#                 lista2.append(valor)
#         print(lista2)
    
#     if opt == 0:
#         break

# OU USANDO FUNÇÕES:

# def calculo_par(x):
#     contador = 0 
#     for numero in x:
#         if numero % 2 == 0:
#             contador += 1
#     return contador

# lista_numeros = []

# while True:
#     digitados = input("Qual número deseja acicionar à lista? Para sair, digite 0: ")
    
#     if digitados == "0":
#        break

#     lista_numeros.append(int(digitados))

# print("Foram passados {} números pares".format(calculo_par(lista_numeros)))
# print(f"Foram passados {calculo_par(lista_numeros)} números pares")

# UMA OUTRA MANEIRA

def calculo_par(x):
    contador = 0 
    for numero in x:
        if numero % 2 == 0:
            contador += 1
    return contador

dados_usuario = list(map(int, input("Digite números, separando-os por vírgula ").split(",")))
print(f"A quantidade de números pares é {calculo_par(dados_usuario)}")



    
