# 1) Crie uma classe que represente um ônibus. O ônibus deverá conter os seguintes atributos:

# capacidade total
# capacidade atual
# movimento

# Os comportamentos esperados para um Ônibus são:
# Embarcar
# Desembarcar
# Acelerar
# Frear

# Lembre-se que a capacidade total do ônibus é de 45 pessoas - não será possível admitir super-
# lotação. Além disso, quando o ônibus ficar vazio, não será permitido efetuar o desembarque
# de pessoas. Além disso, pessoas não podem embarcar ou desembarcar com o onibus em movimento.


class Onibus:
    def __init__(self):
        self.onibus = list()
        self.limite = 45
        self.quantidade = 0
        self.movimento = 0
    
    def embarcar(self, item):
         if self.quantidade < self.limite and self.movimento == 0:
              self.onibus.append(item)
              self.quantidade += 1
              return(f"{item} Passageiro(s) adicionado(s)")

         else:
              return("O ônibus já está cheio ou está em movimento")
    
    def desembarcar(self):
         if self.quantidade > 0 and self.movimento == 0:
              self.onibus.pop(-1)
              self.quantidade -= 1
              return("Passageiro desembarcado")
         else:
              return("O ônibus já está vazio ou está em movimento")

    def acelerar(self):
         self.movimento = 1
         return("O ônibus está em movimento")
    
    def frear(self):
         self.movimento = 0
         return("O ônibus está parado")

    

meu_onibus = Onibus()

# n_pessoas = meu_onibus.quantidade
# subir = meu_onibus.embarcar()
# descer = meu_onibus.desembarcar()

# print(meu_onibus.movimento)
# meu_onibus.acelerar()
# print(meu_onibus.movimento)
# meu_onibus.frear()
# print(meu_onibus.movimento)

# meu_onibus.embarcar(1)
# print(meu_onibus.quantidade)
# meu_onibus.embarcar(1)
# meu_onibus.embarcar(1)
# meu_onibus.embarcar(1)
# meu_onibus.embarcar(1)
# print(meu_onibus.quantidade)

# meu_onibus.acelerar()
# print(meu_onibus.movimento)

