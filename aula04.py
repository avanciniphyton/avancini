#MÓDULOS E FUNÇÕES

#-----------------------------------------Módulos -----------------------------------------------
#São uma maneira de adicionarmos novas funcionalidades / possivilidades para o nosso programa
#Na aula anterior, importamos um módulo chamado OS, que permite que você se comunique com seu OS

#Existem 3 maneiras de termos acesso a módulos no Python:

#1/3 - importando um módulo nativo do Python (ex: módulo OS, math, sqlite3, time)

#import os

# if os.name == "nt":
#     os.system("cls")
# else:
#     os.system("clear")

#2/3 - Instalando módulos com PIP (PIP é um gerenciador de pacotes, similar ao apt no Linux)
#a)No terminal do Python, digitar: pip install flask (instalar o módulo flask)
#b)Após instalação do módulo pelo terminal, importar o módulo no código

#3/3 - Instalando módulos de terceiros (não é seguro)
#a)o módulo vai vir em formato de arquivo
#b)colocar na pasta que estamos usando nosso código
#c)importar o módulo através do comando "import" no seu código

#**Pode-se carregar mais do que um módulo por vez

import os, time, math, sqlite3
time.sleep(5)

#Pode-se importar, também, parte apenas de uma biblioteca de um módulo, e não o módulo
#inteiro, o que economiza bastante em preoessamento

from math import ceil
ceil()

#Podemos criar módulos (como PROCs em JCL e também importar variáveis especídicas de
#dentro de um módulo. Por exemplo, se criarmos um módulo com o seguinte código:

# nome = "Ricardo"

#Há a possibilidde de importar esta variável de dentro deste módulo:

from modulo import nome
print(nome)

#----------------------------------FUNÇÕES-------------------------------------------------------
#São trechos de código que podem ser definidos para serem utilizados e 
#reutilizados no decorrer do meu programa

def limpa_tela():
      if os.name == "nt":
          os.system("cls")
      else:
          os.system("clear")

#Ao definir a função acima, eu consigo chamar esta função no decorrer do meu programa
#e ele irá executar o trecho de código definido na função
          
limpa_tela()
limpa_tela()
limpa_tela()

#Criar uma função para receber o nome de uma pessoa e dar as boas-vindas a ela:

def saudacao(pessoa):
    print(f"Bem vindo ao nosso sistema{pessoa}")

nome1 = input("Qual o seu nome")
nome2 = input("Qual o seu nome")
nome3 = input("Qual o seu nome")

saudacao(nome1)
saudacao(nome2)
saudacao(nome3)

#Criar uma função de soma:

def soma (x, y):
     z = x + y
     print(z)

n1 = 10
n2 = 15

soma(n1,n2)

#Criar uma função de número maior:

def maior():
     if c > 18:
          return True
     else:
          return False

idade = int(input("Qual a sua idade?"))

if maior(idade) == True:
     print("Você é maior de idade")

#Se não sei quantas variáveis vou usar:
lista = [10, 23, 25]
def soma1(*n):
     valor = 0
     for numero in n:
          valor += numero

        return valor

print(soma1(4, 12, 55, 63))
print(soma1(10, 28))

print [lista]

#Mais um exemplo de soma:

def subtracao(a,b):
     c = a - b
     return c

subtracao(10, 15)      #Python vai atribuir os valores de acordo com a posição a,b
subtracao(b=10, a=15)  #Forçando o pgrograma a usar determinada posição para cada argumento

print(subtracao(b=10, a=15))

#Duas maneiras de forçar o usuário a usar sempre uma maneira ou outra dos parâmetros
#Como fazemos isso?

def multiplicacao(*, v1, v2):      #este asterisco simboliza que quando eu tentar utilizar esta
     return v1 * v2                #função, eu digo ao Python que o usuário precisa definir qual
                                   #vai ser o parametro v1 e v2

print(multiplicacao(v1=4, v2=2))


def divisao(f, g, /)               #esta barra simboliza que quando eu tentar urilizar esta
     return f / g                  #função, eu digo ao Pyton que o usuário não possa definir qual
                                   #valor pertence a qual parâmetro. É o oporto do asterisco.
print(divisao(8, 2))

######

def checa_idade(**parametro):             #usamos estes dois asteriecos quando não savemos quantos
     for pessoa, idade in parametro.items():      #parâmetros serão checado
          if idade >= 18:                 #ele armazena as informações em forma de dicionário
               print(f"{pessoa} Maior de idade")  
          else:
               print(f"{pessoa} Menor de idade")

####
#Funções lâmbida / funções anônimas
               
def soma3(x,y):
     return x + y
print(soma3(2,5))

soma4 = lambda x,y: x + y

print(soma4(2,5))
          




          
