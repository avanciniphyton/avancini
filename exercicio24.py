# =================================================================
# 4) Escreva um script em python que represente uma quitanda. O seu programa deverá
# apresentar as opções de frutas, e a cada vez que você escolher a fruta desejada, a fruta
# deverá ser adicionada a uma cesta de compras.

# Menu principal:

# Quitanda:
# 1: Ver cesta
# 2: Adicionar frutas
# 3: Sair

# Menu de frutas:
# Digite a opção desejada:
# Escolha fruta desejada:
# 1 - Banana
# 2 - Melancia
# 3 - Morango

# Digite à opção desejada: 2
# Melancia adicionada com sucesso!

# Os menus 1 e 2 deverão retornar ao menu principal após executar as suas tarefas.
# Você deverá validar as opções digitadas pelo usuário (caso ele digitar algo errado, a mensagem:
# Digitado uma opção inválida

# O programa deverá ser encerrado apenas se o usuário digitar a opção 3.

opt = 0
banana = 0
melancia = 0
morango = 0

while True:
    print(f"""Quitanda
          1: Ver cesta
          2: Adicionar frutas
          3: Sair""")
    opt=int(input("Escolha a sua opção: "))

    if opt == 3:
        print("Obrigado por comprar conosco")
        break

    elif opt == 2:
        print(f"""Menu de Frutas
              1- Banana
              2- Melancia
              3- Morango""")
        opt2=int(input("Qual fruta gostaria de adicionar?"))
        if opt2 == 1:
            banana = banana + 1
        elif opt2 == 2:
            melancia = melancia + 1
        elif opt2 == 3:
            morango = morango + 1
        else:
            print("Opcão inválida, escolha outra opção")
    
    if opt == 1:
        print("Cesta de Compras")
        print("Banana: ", banana)
        print("Melancia: ", melancia)
        print("Morango: ", morango) 
    else:
        print("Opção inváoida, escolha outra opção")    


# OUTRA MANEIRA DE SE RESOLVER
# USANDO O opt !=3 faz a mesma função do "break" ao selecionar a opção 3 no exemplo anterior

# opt = 0
# banana = 0
# melancia = 0
# morango = 0

# while opt !=3:
#     print(f"""Quitanda
#           1: Ver cesta
#           2: Adicionar frutas
#           3: Sair""")
#     opt=int(input("Escolha a sua opção: "))

#     if opt == 3:
#         print("Obrigado por comprar conosco")

#     elif opt == 2:
#         print(f"""Menu de Frutas
#               1- Banana
#               2- Melancia
#               3- Morango""")
#         opt2=int(input("Qual fruta gostaria de adicionar?"))
#         if opt2 == 1:
#             banana = banana + 1
#         elif opt2 == 2:
#             melancia = melancia + 1
#         elif opt2 == 3:
#             morango = morango + 1
#         else:
#             print("Opcão inválida, escolha outra opção")
    
#     if opt == 1:
#         print("Cesta de Compras")
#         print("Banana: ", banana)
#         print("Melancia: ", melancia)
#         print("Morango: ", morango) 

