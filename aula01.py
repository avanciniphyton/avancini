# PRINT é usado para quando queremos mostrar algo para o usuário

print ("Olá, bem vindo ao curso de Python")

# INPUT é usado para ler os dados do usuário

# input ("Qual seu nome?")

# VARIÁVEIS são usadas para definir nomes dentro do Python
# nome = Ricardo ou nome = input("Qual seu nome?")
# print ("Seu nome é:", nome)

nome = input("Qual seu nome?")
print("Seu nome é:", nome)
