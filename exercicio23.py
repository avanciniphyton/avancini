# 3) Escreva um programa que receba o ano de nascimento, e que ele retorne à geração
# a qual a pessoa pertence. Para definir a qual geração uma pessoa pertence temos a
# seguinte tabela:

# Geração        Intervalo

# Baby Boomer -> Até 1964
# Geração X   -> 1965 - 1979
# Geração Y   -> 1980 - 1994
# Geração Z   -> 1995 - Atual

# Para testar se seu script está funcionando, considere os seguintes resultados esperados:

# • ano nascimento = 1988: Geração: Y
# • ano nascimento = 1958: Geração: Baby Boomer
# • ano nascimento = 1996: Geração: Z

nascimento = int(input("Qual seu ano de nascimento? "))

if nascimento <= 1964:
    print("Você pertence à Geração Baby Boomer")
elif nascimento <= 1979:
    print("Você pertence à Geração X")
elif nascimento <= 1994:
    print("Você pertence à Geração Y")
else:
    print("Você pertence à Geração Z")