#Classes -> tipos de objetos

# str()
# int()
# float()
# bool()
# tuple()
# list()
# dict()
# set()

#Vamos criar uma classe que represente uma pilha.

class Pilha:
    def __init__(self):
        self.pilha = list
        self.limite = 5
        self.quantidade = 0
    
    def empilhar(self, item):
        if self.quantidade < self.limite:
            self.pilha.append(item)
            self.quantidade += 1
            return(f"{item} adicionado à pilha")
        else:
            return(f"Esta pilha já possui {self.limite} itens.")
    
    def desempilhar(self):
        if self.quantidade > 0:
            item2 = self.pilha[-1]
            self.pilha.pop(-1)
            self.quantidade -= 1
            return(f"Item {item2} renovido da pilha")
        else:
            return("A pilha já está vazia")

#Com isso podemos ter vários objetos utilizando-se da mesma classe "Pilha".

pilha_de_pratos = Pilha()
pilha_de_brinquedos = Pilha()
pilha_de_roupas = Pilha()

print(pilha_de_pratos.limite)

# print(pilha_de_pratos.empilhar("Prato de Vidro"))
# print(pilha_de_pratos.empilhar("Prato de Porcelana"))
# print(pilha_de_pratos.empilhar("Prato de Madeira"))
# print(pilha_de_pratos.empilhar("Prato de Metal"))
# print(pilha_de_pratos.empilhar("Prato de Borracha"))


#=========================

#Encapsulamento -> dados não poderão ser acessados de fora de nosso objeto

class Pilha2:
    def __init__(self):
        self.__pilha = list
        self.__limite = 5
        self.__quantidade = 0
    
    def empilhar(self, item):
        if self.__quantidade < self.limite:
            self.__pilha.append(item)
            self.__quantidade += 1
            return(f"{item} adicionado à pilha")
        else:
            return(f"Esta pilha já possui {self.limite} itens.")
    
    def desempilhar(self):
        if self.__quantidade > 0:
            item2 = self.pilha[-1]
            self.__pilha.pop(-1)
            self.__quantidade -= 1
            return(f"Item {item2} renovido da pilha")
        else:
            return("A pilha já está vazia")

pilha_de_cartas = Pilha2()
print(pilha_de_cartas.__limite)

#================================================

#Herança

#é muito comum estar trabalhando com múltiplas classes, e em alguns casos algumas
#classes irão herdar informações de outras classes

class Funcionario:
    def __init__(self):
        self.salario = 0
        self.nome = ""

    def define_nome(self, x):
        self.nome = x

    def define_salario(self, y):
        self.salario = y
    

class Gerente(Funcionario):
    def __init__(self):
        self.bonus = 1.3
    
    def aplica_bonus(self):
        self.salario = self.salario * self.bonus

joao = Funcionario()
joao.define_nome("João")
joao.define_salario(3000.00)
print(joao.salario)

pedro = Gerente()
pedro.define_nome("Pedro")
pedro.define_salario(3000.00)
pedro.aplica_bonus()
print(pedro.salario)

#=====================================================

#Polimorfismo -> altwrar o valor recebido de uma determinada classe herdada

class Funcionario2:
    def __init__(self):
        self.salario = 3000.00
        self.nome = ""

    def define_nome(self, x):
        self.nome = x
    

class Gerente(Funcionario2):
    def __init__(self):
        self.salario = 4500      #Alteramos o valor do saário de 2000.00 para 4500.00







