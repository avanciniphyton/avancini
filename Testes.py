# lista = []          #CRIAR UMA LISTA VAZIA
# print(lista)
# lista.append(2)     #ADICIONAR O CARACTERE "2" AO FINAL DA LISTA
# print(lista)
# lista.append(3)     #ADICIONAR O CARACTERE "3" AO FINAL DA LISRA
# print(lista)
# lista.insert(0,1)   #INSERIR O CARACTERE "1" NA POSIÇÃO "0" DA LISTA (INSERT(POS,VALUE))
# print(lista)
# lista.insert(4,4)   #INSERIR O CARACTERE "4" NA POSIÇÃO "4" LISTA (INSERT(POS,VALUE))
# print(lista)
# lista.reverse()     #SORT NOS VALORES DA LISTA DE MANEIRA DECRESCENTE
# print(lista)
# lista.sort()        #SORT NOS VALORES DA LISTA DE MANEIRA CRESCENTE
# print(lista)
# lista.pop()        #REMOVER O ÚLTIMO ÍTEM DA LISTA
# print(lista)
# lista.pop(0)        #REMOVER UM ÍTEM ESPECÍFICO DA LISTA, NESTE CASO O ÍTEM DA POSIÇÃO 0
# print(lista)

#========================================

# def soma (x, y):
#      z = x + y
#      return z

# n1 = int(input("Escolha um número"))
# n2 = int(input("Escolha mais um número"))

# print(f"A soma dos dois valores é: {soma(n1,n2)}")

# lista = [1,2,3]
# lista.insert(0,4)
# print(lista)

# txt = "Python"
# x = txt.upper()
# print(x)

# def exibir_menu():
#     print("Bem vindo à lanchonete!")
#     print("Aqui estão nossas opções:")
#     print("1 - Comida 1 - R$ 17,00")
#     print("2 - Comida 2 - R$ 13,00")
#     print("3 - Comida 3 - R$ 20,00")
#     print("4 - Comida 4 - R$ 18,50")
#     print("5 - Comida 5 - R$ 14,50")
#     print("6 - Bebida 1 - R$ 06,00")
#     print("7 - Bebida 2 - R$ 05,00")
#     print("8 - Bebida 3 - R$ 10,00")

# exibir_menu()

import os

