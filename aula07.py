# Executar conteúdo apenas quando o arquivo for rodado diretamente:

if __name__ == "__main__":   #tudo o que estiver abaixo da linha 3 não será eecutado
    pass                     #quando o arquivo for chamado.

#==================================================================

# Exceções

# Se executarmos o seguinte código:

# nome = int("abc")

#receberemos um ValueError, uma vez que o int vai aguardar um número inteiro, e temos uma string
#para dizermos ao Python como lidar com este erro, fazemos o seguinte:

try:                                #tente executar este código
    nome = int("abc")

except ValueError:                  #Se der a exceção X, faça o seguinte:
    print("O programa deu uma exceção de ValueError, tente um valor diferente")

except ValueError:
    print()

else:                               #Se não der nenhum erro, execute:
    print("Não deu nenhum erro")

finally:                            #Finally sempre executa no final:
    print("Sempre execute no final")

#=================================================================

#Bancos de Datos -> São aplicaçÕes utilizadas para armazenamento de dados

#Já vimos antes como adicionar dados a arquivos de texto, csv, por exemplo, mas estas não são
#maneiras efetivas e/ou seguras de se armazenar dados, mas sim através de Bancos de Dados.

#SQL -> Armazena informações em forma de tabela
    #é um frormato de banco de dados relacional
    #temos bancos de dados não relacionais no Python (ex: mongoDB)

import sqlite3            #maneira de se importar o módulo sqlite3

conexão = sqlite3.connect("empresa.db")  #Cria uma conexão com o banco e o cria, caso não exista
cursor = conexão.cursor()                #Utilizado para editar o banco

# Vamos criar uma tabela dentro do banco de dados empresa.db

comando_cria_tabela = """""
CREATE TABLE funcionarios (
id integer primary key autoincrement,  
nome text,
idade integer
)"""

#Executamos este comando com o cursor

cursor.execute(comando_cria_tabela)

try:
    # Executamos esse comando com o cursor:
    cursor.execute(comando_cria_tabela)
except sqlite3.OperationalError:
    pass

# Criar um comando pra adicionar valores em uma tabela no banco de dados:
comando_adiciona_funcionario1 = """
INSERT INTO funcionarios (nome, idade) 
VALUES ("Tiago", 27)
"""

comando_adiciona_funcionario2 = """
INSERT INTO funcionarios (nome, idade) 
VALUES ("Caio", 28)
"""

comando_adiciona_funcionario3 = """
INSERT INTO funcionarios (nome, idade) 
VALUES ("Kaique", 30)
"""

cursor.execute(comando_adiciona_funcionario1)
cursor.execute(comando_adiciona_funcionario2)
cursor.execute(comando_adiciona_funcionario3)
conexao.commit()

# Criar um comando pra remover valores de uma tabela no banco de dados:
comando_remove_item = 'DELETE FROM funcionarios WHERE nome = "Tiago"'
cursor.execute(comando_remove_item)
conexao.commit()

# Criar um comando pra atualizar um valor em uma tabela no banco de dados:
comando_atualiza_tabela = 'UPDATE funcionarios SET idade = 35 WHERE id = 2'
cursor.execute(comando_atualiza_tabela)
conexao.commit()

# Criar um comando pra checar o conteudo de uma tabela no banco de dados:
comando_checa_tabela1 = 'SELECT * FROM funcionarios' # Coleta todas as colunas e todas as linhas
comando_checa_tabela2 = 'SELECT idade FROM funcionarios' # Coleta a coluna idade e todas as linhas
comando_checa_tabela3 = 'SELECT * FROM funcionarios WHERE nome = "Kaique"' # Coleta todas as colunas na linha idade

cursor.execute(comando_checa_tabela1)
conteudo = cursor.fetchall()

for linha in conteudo:
    print(linha)
