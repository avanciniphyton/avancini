# Crie um script para simular uma lanchonete.
# 
# - Essa lanchonete deverá ter 5 tipos de comida e 3 tipos de bebidas no cardápio.
# - Quando clientes chegarem a essa lanchonete, eles deverão fazer um pedido aleatório de 1 comida + 1 bebida.
# - A lanchonete deverá ter um numero limitado de porções dessas comidas/bebidas. Comida = 5, bebida = 7.
# - Quando o cliente fazer o pedido, ele deverá ser notificado se as opções escolhidas estão disponiveis,
# e quais seus preços.

# Criar um DB.
# Criar uma tabela pra comida e uma pra bebida.
# Preencher essas tabelas.

# Criar uma função pra simular um cliente comprando algo.

# import sqlite3
# from random import randint, choice

# #criar o DB chamado lanchonete
# conexão = sqlite3.connect("lanchonete.db") 
# cursor = conexão.cursor()

# # #Criar tabela para comida
# cria_tabela_comida = """
# CREATE TABLE comidas (
# id integer primary key autoincrement,
# tipo text,
# preco real,
# quantidade integer
# )"""

# #Criar tabela para bebida
# cria_tabela_bebida = """
# CREATE TABLE bebidas (
# id integer primary key autoincrement,
# tipo text,
# preco real,
# quantidade integer
# )"""

# cursor.execute(cria_tabela_comida)
# cursor.execute(cria_tabela_bebida)

# #Preencher a tabela de comidas com 5 tipos de comida e suas quantidades

# adiciona_comida1 = """
# INSERT INTO comidas (tipo, preco, quantidade) 
# VALUES ("Comida1", 17.00, 5)
# """

# adiciona_comida2 = """
# INSERT INTO comidas (tipo, preco, quantidade) 
# VALUES ("Comida2", 13.00, 5)
# """

# adiciona_comida3 = """
# INSERT INTO comidas (tipo, preco, quantidade) 
# VALUES ("Comida3", 20.00, 5)
# """

# adiciona_comida4 = """
# INSERT INTO comidas (tipo, preco, quantidade) 
# VALUES ("Comida4", 18.50, 5)
# """

# adiciona_comida5 = """
# INSERT INTO comidas (tipo, preco, quantidade) 
# VALUES ("Comida5", 14.50, 5)
# """

# cursor.execute(adiciona_comida1)
# cursor.execute(adiciona_comida2)
# cursor.execute(adiciona_comida3)
# cursor.execute(adiciona_comida4)
# cursor.execute(adiciona_comida5)
# conexão.commit()

# #Preencher a tabela de bebidas com 3 tipos de bebida e suas quantidades

# adiciona_bebida1 = """
# INSERT INTO bebidas (tipo, preco, quantidade) 
# VALUES ("Bebida1", 6.50, 7)
# """

# adiciona_bebida2 = """
# INSERT INTO bebidas (tipo, preco, quantidade) 
# VALUES ("Bebida2", 5.00, 7)
# """

# adiciona_bebida3 = """
# INSERT INTO bebidas (tipo, preco, quantidade) 
# VALUES ("Bebida3", 10.00, 7)
# """

# cursor.execute(adiciona_bebida1)
# cursor.execute(adiciona_bebida2)
# cursor.execute(adiciona_bebida3)
# conexão.commit()

# #checar se as tabelas estão corretas:

# comando_checa_comidas = 'SELECT * FROM comidas'

# cursor.execute(comando_checa_comidas)
# conteudo_comidas = cursor.fetchall()

# print(conteudo_comidas)


# comando_checa_bebidas = 'SELECT * FROM bebidas'

# cursor.execute(comando_checa_bebidas)
# conteudo_bebidas = cursor.fetchall()

# print(conteudo_bebidas)

# #Criando uma função para simular um cliente comprando algo

# def exibir_menu():
#     print("Bem vindo à lanchonete!")
#     print("Aqui estão nossas opções:")
#     print("1 - Comida 1 - R$ 17,00")
#     print("2 - Comida 2 - R$ 13,00")
#     print("3 - Comida 3 - R$ 20,00")
#     print("4 - Comida 4 - R$ 18,50")
#     print("5 - Comida 5 - R$ 14,50")
#     print("6 - Bebida 1 - R$ 06,00")
#     print("7 - Bebida 2 - R$ 05,00")
#     print("8 - Bebida 3 - R$ 10,00")
#     print("9 - Verificar total")
#     print("0 - Sair")

# def fazer_pedido(escolha):
#     qtde_escolhida = 0
#     total = []
#     while qtde_escolhida <= 2:
#         print(input("Escolha uma opção de comida e uma opção de bebida"))
#         if escolha == 1:
#             qtde_escolhida += 1
#             total.append(float(17.00))
#             cursor.execute("UPDATE comidas SET quantidade = -1 WHERE id = 1")
        
#         if escolha == 2:
#             qtde_escolhida += 1
#             total.append(float(13.00))
#             cursor.execute("UPDATE comidas SET quantidade = -1 WHERE id = 2")

#         if escolha == 3:
#             qtde_escolhida += 1
#             total.append(float(20.00))
#             cursor.execute("UPDATE comidas SET quantidade = -1 WHERE id = 3")
        
#         if escolha == 4:
#             qtde_escolhida += 1
#             total.append(float(18.50))
#             cursor.execute("UPDATE comidas SET quantidade = -1 WHERE id = 4")

#         if escolha == 5:
#             qtde_escolhida += 1
#             total.append(float(14.50))
#             cursor.execute("UPDATE comidas SET quantidade = -1 WHERE id = 5")

#         if escolha == 6:
#             qtde_escolhida += 1
#             total.append(float(06.00))
#             cursor.execute("UPDATE comidas SET quantidade = -1 WHERE id = 6")

#         if escolha == 7:
#             qtde_escolhida += 1
#             total.append(float(05.00))
#             cursor.execute(f"UPDATE comidas SET quantidade = -1 WHERE id = 7")

#         if escolha == 8:
#             qtde_escolhida += 1
#             total.append(float(10.00))
#             cursor.execute("UPDATE comidas SET quantidade = -1 WHERE id = 8") 
        
#         if escolha == 9:
#             total_compra = sum(total)
#             print(f"O total da sua compra é de R$ {total_compra}")
        
#         if escolha == 0:
#             break
    
#     print("Obrigado por sua visita. Volte sempre")


#===========================================================================
#CORREÇÃO DO PROFESSOR

import sqlite3
from random import choice

conexao = sqlite3.connect("estoque.db")
cursor = conexao.cursor()

tabela_comida = """
CREATE TABLE comidas (
id integer primary key autoincrement, 
nome, 
preco, 
quantidade)"""

tabela_bebida = """
CREATE TABLE bebidas (
id integer primary key autoincrement, 
nome, 
preco, 
quantidade)
"""

try:
    cursor.execute(tabela_comida)
    cursor.execute(tabela_bebida)

    comidas = [("Hamburger", 25.00, 5), ("Hotdog", 12.00, 5), ("Chocolate", 5.00, 5), ("Pão", 1.50, 5), ("Misto quente", 7.50, 5)]
    bebidas = [("Coca cola", 10.00, 7), ("Fanta", 8.00, 7), ("Café", 2.50, 7)]

    for comida in comidas:
        regristar_comida = f'INSERT INTO comidas (nome, preco, quantidade) VALUES ("{comida[0]}", "{comida[1]}", "{comida[2]}")'
        cursor.execute(regristar_comida)

    for bebida in bebidas:
        regristar_bebida = f'INSERT INTO bebidas (nome, preco, quantidade) VALUES ("{bebida[0]}", "{bebida[1]}", "{bebida[2]}")'
        cursor.execute(regristar_bebida)

    conexao.commit()

except sqlite3.OperationalError:
    pass

def checa_info(tabela, coluna, pedido):
    checa_estoque = f'SELECT {coluna} FROM {tabela} WHERE nome = "{pedido}"'
    cursor.execute(checa_estoque)
    conteudo = cursor.fetchall()

    return conteudo[0][0]

def cliente():
    coleta_comidas = "SELECT nome FROM comidas"
    cursor.execute(coleta_comidas)
    lista_comidas = cursor.fetchall()

    coleta_bebidas = "SELECT nome FROM bebidas"
    cursor.execute(coleta_bebidas)
    lista_bebidas = cursor.fetchall()

    pedido = {}
    pedido["comida"] = choice(lista_comidas)[0]
    pedido["bebida"] = choice(lista_bebidas)[0]

    quantidade_atual_comida = int(checa_info("comidas", "quantidade", pedido["comida"]))
    quantidade_atual_bebida = int(checa_info("bebidas", "quantidade", pedido["bebida"]))

    if quantidade_atual_comida == 0:
        print(f'O item {pedido["comida"]} está fora de estoque.')
  
    elif quantidade_atual_bebida == 0:
        print(f'O item {pedido["bebida"]} está fora de estoque.')

    else:
        atualiza_quantidade_comida = f'UPDATE comidas SET quantidade = {quantidade_atual_comida - 1} WHERE nome = "{pedido["comida"]}"'
        atualiza_quantidade_bebida = f'UPDATE bebidas SET quantidade = {quantidade_atual_bebida - 1} WHERE nome = "{pedido["bebida"]}"'

        cursor.execute(atualiza_quantidade_comida)
        cursor.execute(atualiza_quantidade_bebida)
        conexao.commit()

        preco_comida = float(checa_info("comidas", "preco", pedido["comida"]))
        preco_bebida = float(checa_info("bebidas", "preco", pedido["bebida"]))
        preco_total = preco_comida + preco_bebida

        print(f'Pedido feito! {pedido["comida"]} + {pedido["bebida"]}! Preço: R${preco_total:.2f}')

for x in range(0, 30):
    cliente()
