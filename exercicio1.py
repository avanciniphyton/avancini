# 1) Em muitos programas nos é solicitado o preenchimento de algumas
# informações, como nome, cpf, idade e unidade federativa. Escreva um script
# em Python que solicite as informações cadastrais mencionadas e que,
# em seguida, as apresente da seguinte forma:
# 
# ------------------------
# Confirmação de Cadastro:
# Nome: Guido Van Rossun
# CPF: 999.888.777/66
# Idade: 65
# ------------------------

print("Olá, seja vem vindo ao nosso sistema")
nome = input("Qual é o seu nome? ")
cpf = input("Qual é o seu CPF? ")
idade = input("Qual a sua idade? ")
print("Confirmação de Cadastro:")
print("Nome:", nome)
print("CPF:", cpf)
print("Idade:", idade)

#OU

print("Confirmação de Cadastro:")
print(f"Nome: {nome}")
print(f"CPF: {cpf}")
print (f"Idade: {idade}")

#OU

print("""Confirmação de Cadastro:
Nome: {nome}
CPF: {cpf}
Idade: {idade} """)
