# Exercicio 2:
# Escreva uma calculadora utilizando funções
# Ela pergunta dois numeros, e da as opções de calculo.
# (soma, diferença, multiplicação, divisão)

def Somar(b,c):
    a = b + c
    print(f"Seu resultado é: {a}")

def Subtrair(d,e):
    f = d - e
    print(f"Seu resultado é: {f}")

def Multiplicar(g,h):
    i = g * h
    print(f"Seu resultado é: {i}")

def Dividir(j,k):
    l = j / k
    print(f"Seu resultado é: {l}")

n1 = int(input("Digite o primeiro número "))
n2 = int(input("Digite o segundo número "))
opt = ()

while True:
    print(f"""O que deseja fazer com tais números?
      
      Somar
      Subtrair
      Multiplicar
      Dividir
      Sair 
          """)
      
    opt = (input("Digite sua opção: "))
    
    if opt == "Sair":
        break
    
    elif opt == "Somar":
        Somar(n1, n2)
    
    elif opt == "Subtrair":
        Subtrair(n1, n2)
 
    elif opt == "Multiplicar":
        Multiplicar(n1, n2)

    elif opt == "Dividir":
        Dividir(n1, n2)