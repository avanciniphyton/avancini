#Persistência de dados
#Todos os dados que criamos durante a execução do nosso script fica somente na memória RAM do
#computador, porém quando desligamos a máquina, nós perdemos tais informações.

#Hoje vamos ver uma maneira mais simples de persistir tais dados, salvando em disco rígido e não
#somente em memória ram.

# Persistência de dados com o OPEN

# nome = input("Qual o seu nome?")

# # arquivo = open("nomes.txt")                    #CASO O ARQUIVO NÃO EXISTA, O SISTEMA IRÁ CRIÁ-LO
# # arquivo2 = open(r"C:\WINDOWS\etc\nomes.txt")   #o "r" na frente do texto ignora caracteres especiais

# #modos de abrir arquivos
# #w = write - escrever (se o seu arquivo tiver algo no arquivo, o Python irá sobrescrever)
# #r = read - ler
# #a = append - adicionar algo ao final arquivo (não apaga o que já existe no arquivo)

# arquivo = open("nomes.txt", "a")   #criamos o arquivo
# arquivo.write(f"{nome}\n")         #escrevemos algo no arquivo e pulamos uma linha
# arquivo.close()                    #fechamos o arquivo após o uso

# arquivo = open("nomes.txt", "r")
# conteudo = arquivo.read()          #pego o que está sendo lido no meu arquivo
# arquivo.close()
# print(conteudo)

# with open("nomes.txt", "a") as arquivo:       #com isso você não precisa fechar a conexão com o close
#     arquivo.write(f"testando o with open\n")

#===============================

#csv = é um arquivo de texto que é estruturado, de maneira a poder ser convertido facilmente para
#uma planilha se necessário.
    
import csv

with open("planilha.csv", "r") as arquivo2:
    conteudo2 = csv.reader(arquivo2, delimiter=";")     #ler arquvos csv

    for linha in conteudo2:
        print(linha)


with open("planilha.csv", "a") as arquivo2:
    conteudo2 = csv.writer(arquivo2, delimiter=";")     #escrever em arquvos csv

    for linha in conteudo2:
        print(linha)

