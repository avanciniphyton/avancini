#Lists:
def cria_lista(s):
    lista = []
    for numero in range(0, s):
        lista.append(numero)
    return lista

lista1 = cria_lista(5)
for x in lista1:
    print(x)

# Generators:
def cria_generator(s):
    for numero in range(0, s):
        yield numero

gen = cria_generator(5)
for x in gen:
    print(x)

# Generator Comprehensions:
def gen():
    for x in range(0, 10):
        yield x

gencomp = gen()
print(next(gencomp))

gencomp2 = (x for x in range(0, 10))
print(next(gencomp2))

# List Comprehensions:
def dobro_lista(x):
    lista = []
    for numero in range(0, x):
        lista.append(numero * 2)
    return lista

print(dobro_lista(10))

lista_comp = [x * 2 for x in range(0, 10)]
print(lista_comp)

# Dict comprehensions:
def gera_dict():
    dicionario = {}
    for chave, valor in enumerate(range(550, 560)):
        dicionario[chave] = valor
    return dicionario

dici = gera_dict()
print(dici)

dict_comp = {chave: valor for chave, valor in enumerate(range(550, 560))}
print(dict_comp)

# =======================================
# map

def quadrado(x):
    return x**2

quadrado2 = lambda x: x**2

lista = [1, 2, 3, 4, 5]

resultado = map(quadrado2, lista)
print(list(resultado))

# reduce
from functools import reduce
lista = [5, 10, 15, 20, 25]

def soma(x, y):
    return x + y

soma2 = lambda x, y: x + y

print(reduce(soma, lista))

# filter
temperaturas = [25, 30, 35, 36, 12, 21, 41, 22, 23, 36]

sub30 = lambda x: x < 30

print(list(filter(sub30, temperaturas)))
