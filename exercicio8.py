# ====================================================================================
# 1) Escreva um programa utilizando funções que realize um cadastro.
# Deverão ser coletadas as seguintes informações:

# CPF
# Nome
# Idade
# Sexo
# Cidade

# Os registros deverão ser armazenados em um arquivo CSV.
# Para manter o padrão brasileiro, o CSV será separado pelo caractere ";".
# O programa deverá possuir uma função de consulta e de exclusão (POR NOME OU CPF).
# O programa deverá possuir tratamentos de erro, com a finalidade de que o programa nunca
# dê uma exceção e também que ele não aceite dados incorretos em nenhum momento.

# 1: Importar os modulos a serem usados.
# 2: Criamos uma função para o menu principal do programa.
# 3: Criar uma função para coletar as informações do cliente.
# 4: Criar uma função para validar as informações passadas pelo cliente.
# 5: Criar uma função para adicionar essas informações em um arquivo CSV.
# 6: Criar uma função para consultar o arquivo CSV.
# 7: Criar uma função para remover entradas do arquivo CSV.

import csv, os

def menu():
    while True:
        opcao = input("""
O que deseja fazer?
1 - Consultar um valor no banco.
2 - Cadastrar um valor no banco.
3 - Remover uma valor do banco.
4 - Parar o programa.\n\n""")
        
        os.system("cls" if os.name == "nt" else "clear")

        if opcao == "4":
            print("Parando o programa...")
            break

        elif opcao == "3":
            exclusao()

        elif opcao == "2":
            cadastro()

        elif opcao == "1":
            consulta()

        else:
            print("Voce inseriu um valor inválido.\n")

def cadastro():
    while True:
        while True:
            cpf = input("Qual o cpf dessa pessoa? ")
            if validacao("cpf", cpf) == False:
                print("CPF inválido, digite novamente.")
                continue
            break

        while True:
            idade = input("Qual a idade da pessoa? ")
            if validacao("idade", idade) == False:
                print("Idade inválida, digite novamente.")
                continue
            break

        nome = input("Qual é o nome da pessoa? ")
        sexo = input("Qual o sexo da pessoa? ")
        cidade = input("Qual a cidade onde essa pessoa reside? ")

        dados_a_cadastrar = [cpf, nome, idade, sexo, cidade]

        with open("banco.csv", "a", newline="") as arquivo:
            writer = csv.writer(arquivo, delimiter=";")
            writer.writerow(dados_a_cadastrar)

        print("Individuo cadastrado com sucesso.")
        resposta = input("\nDeseja adicionar outro valor? [S/N]: ")
        if resposta.upper() == "N":
            os.system("cls" if os.name == "nt" else "clear")
            break

        os.system("cls" if os.name == "nt" else "clear")

def validacao(teste, valor):
    match teste:

        case "cpf":
            cpf = valor.replace(".", "").replace("-", "").strip()
            if len(cpf) == 11 and cpf.isnumeric() == True:
                return True
            else:
                return False
            
        case "idade":
            idade = valor.strip()
            if idade.isnumeric() == True:
                if int(idade) < 130 and int(idade) > 0:
                    return True
                else:
                    return False
            else:
                return False
            
def consulta():
    while True:
        resposta = input("""Por qual valor deseja consultar?
0 - CPF
1 - Nome\n\n""")
        
        if resposta in "01":
            resposta = int(resposta)
            break

        else:
            print("Opção inválida, digite novamente.")
    
    os.system("cls" if os.name == "nt" else "clear")

    while True:
        cpf_nome = input("Digite o nome ou CPF a consultar: ")
        if resposta == 0:
            if validacao("cpf", cpf_nome) == False:
                print("CPF inválido, digite novamente.")
                continue
            else:
                break
        else:
            break

    os.system("cls" if os.name == "nt" else "clear")

    with open("banco.csv", "r") as arquivo:
        conteudo = csv.reader(arquivo, delimiter=";")
        valor_encontrado = False

        for linha in conteudo:
            if linha[resposta] == cpf_nome:
                print(f"""Valor encontrado:
CPF: {linha[0]}
Nome: {linha[1]}
Idade: {linha[2]}
Sexo: {linha[3]}
Cidade: {linha[4]}
""")
                valor_encontrado = True
                break
        
        if valor_encontrado == False:
            print("Valor não encontrado.")

def exclusao():
    cpf_nome = input("Digite o CPF ou nome da pessoa a excluir: ")

    with open("banco.csv", "r") as arquivo_inicial, open("banco_editado.csv", "a", newline="") as arquivo_editado:
        writer = csv.writer(arquivo_editado, delimiter=";")
        reader = csv.reader(arquivo_inicial, delimiter=";")

        for linha in reader:
            if linha[0] != cpf_nome and linha[1] != cpf_nome:
                writer.writerow(linha)

    os.remove("banco.csv")
    os.rename("banco_editado.csv", "banco.csv")


if __name__ == "__main__":
    menu()
