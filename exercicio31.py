# import os

# ======================================================================================================
# 1) Altere o exercicio 2.4 para adicionar o preço dos itens comprados, mantendo uma conta do valor
# total gasto nas compras, e no fim, imprima o valor total e os itens na cesta de compras.
# Utilize coleções para manter o conteudo da cesta de compras.
# Opcional, mantenha o terminal limpo sempre que possivel.

# if os.name == "nt":
#     os.system("cls")
# else:
#     os.system("clear")


opt = 0
banana = 0
melancia = 0
morango = 0

# valor1 = {"Banana":5}
# valor2 = {"Melancia":10}
# valor3 = {"Morango":4}
valores = {"1: Banana - R$":5.00, "2: Melancia - R$":10.00, "3: Morango - R$":4.00}

while True:
    print(f"""Quitanda
          1: Ver cesta
          2: Adicionar frutas
          3: Sair""")
    opt=int(input("Escolha a sua opção: "))

    if opt == 3:
        resultado1 = banana * valores["1: Banana - R$"]
        resultado2 = melancia * valores["2: Melancia - R$"]
        resultado3 = morango * valores["3: Morango - R$"]
        total = resultado1 + resultado2 + resultado3
        print("Você gastou R$ {}".format(total))
        print("Obrigado por comprar conosco")
        break

    elif opt == 2:
        for x,y in valores.items():
            print(x,y)
        # print(f"""Menu de Frutas
        #       1- Banana R$ 5.00
        #       2- Melancia R$ 10.00
        #       3- Morango R$ 4.00
        #       """)
        opt2=int(input("Qual fruta gostaria de adicionar?"))

        if opt2 == 1:
            banana = banana + 1
        elif opt2 == 2:
            melancia = melancia + 1
        elif opt2 == 3:
            morango = morango + 1
        else:
            print("Opcão inválida, escolha outra opção")
    
    if opt == 1:
        print("Cesta de Compras")
        print("Banana: ", banana)
        print("Melancia: ", melancia)
        print("Morango: ", morango) 
else:
    print("Opção inváLida, escolha outra opção")    