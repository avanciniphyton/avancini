# Exercicio 4:
# Assumindo que uma lata de tinta pinta 3m², escreva um programa
# que possua uma função que receba as dimenções de uma parede,
# passadas pelo usuario, calcule sua área, e mostre uma mensagem
# dizendo quantas latas de tinta seriam necessárias para pintar
# essa parede.

# dimensoes = []

# dimensoes.append(int(input("Qual a largura da sua parede em metros? ")))
# dimensoes.append(int(input("Qua a altura da sua parede em metros? ")))

# latas = (dimensoes[0] * dimensoes[1]) / 3
# print(f"Você vai precisar de {latas} latas de tinta")

# OU USANDO FUNÇÕES

# def calculo_tinta(n1, n2):
#     dados = n1 * n2
#     quantidade = (dados / 3)
#     return quantidade

# n1 = float(input("Qual a largura da sua parede em metros? "))
# n2 = float(input("Qual a altura da sua parede em metros? "))

# num_latas = calculo_tinta(n1, n2)

# print(f"Você vai precisar de {num_latas} latas de tinta")

# OU USANDO O MÓDULO MATH.CEIL (QUE ARREDONDA UM NÚMERO PARA MAIS INTEIRO)

from math import ceil

def calculo_tinta(n1, n2):
    dados = n1 * n2
    quantidade = ceil(dados / 3)
    return quantidade

n1 = float(input("Qual a largura da sua parede em metros? "))
n2 = float(input("Qual a altura da sua parede em metros? "))

num_latas = calculo_tinta(n1, n2)

print(f"Você vai precisar de {num_latas} latas de tinta")



