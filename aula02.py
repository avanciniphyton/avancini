#Tipos Primitivos

# String      -> Str    -> Frases, conjuntos de caracteres, sempre com aspas
# Integer     -> int    -> números inteiros, sem aspas
# Float       -> float  -> números decimais, números reais com ponto, sem aspas
# Boolean     -> bool   -> True or False, sem aspas

# ==========================================

nome = "Ricardo Avancini"  # String
peso = "91"                # String
idade = 37                 # Integer
altura = 1.73              # Float
moreno = False             # Boolean

numero1 = 10
numero2 = 15

numero3 = numero1 + numero2
print(numero3)

# ==========================================

# Um dado recebido de um input vai ser sempre armazanado como String
# Porém temos como converter uma variável de Integer

idade = input("Qual a sua idade? ")
idade = int(idade)
idade = int(input("Qual a sua idade? "))

# ==========================================

#Capitalize vai colocar a primeira letra de ums string como maiúscula
nome = "ricardo avancini".capitalize()

#Title vai colocar as primeiras letras de uma string como maiúscula
nome = "ricardo avancini".title()

#Upper vai colocar todas as letras de uma string para maiúscula
nome =  "ricardo avancini".upper()

#Replace vai substituir letras de uma string
nome = "ricardo avancini".replace("o", "a")

#ISALNUM serve para checar se uma string possui apenas letras e números
print(nome.isalnum())

#ISLOWER serve para checar se 
print(nome.islower())

# ==========================================

# Estruturas de decisão

idade = int(input("Qual a sua idade? "))
if idade >= 18:
    print("Você pode entrar e comprar bebidas no bar")
    print("Aproveite a noite")

elif idade >= 15:
    print("Você pode entrar, mas não pode comprar bebidas no bar!")

elif idade >= 13:
    print("Você precisa entrar acompanhado de um adulto")

else:
    print("Você é muito jovem para entrar!")

# ===========================================
    
# Estruturas de repetição

# Utilizaremos para isso os comandos FOR e o WHILE

# loop definido

contador = 10
while contador != 0:
    print("Estou contando...")
    print("Executando código...")
    contador = contador - 1

# loop infinito

while True:
    resposta = input("Quer parar o programa? [s/n] ")
    if resposta == "s":
        break

# loop dentro de outro
    
while True:
    resposta = input("Quer parar o programa? [s/n] ")
    if resposta == "s":
        break

    while True:
        print("teste")
        break

# loop com o FOR - tem mais controle sobre ele do que o WHILE
    
for var1 in range(0,10):
    print(f"Imprimindo a {var1}a mensagem")